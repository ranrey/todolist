package ru.ranrey.sindev.todolist.ui.mainScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.ranrey.sindev.todolist.domain.ToDoItemInteractor

class MainViewModelFactory(private val toDoItemInteractor: ToDoItemInteractor): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(toDoItemInteractor) as T
    }
}