package ru.ranrey.sindev.todolist

import android.app.Application
import ru.ranrey.sindev.todolist.data.database.AppDatabase

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        AppDatabase.getAppDataBase(baseContext)
    }

    override fun onTerminate() {
        super.onTerminate()
        AppDatabase.destroyDataBase()
    }
}