package ru.ranrey.sindev.todolist.ui.util

class OneShootEvent<out T>(private val content: T? = null) {

    var hasBeenHandled = false
    private set

    private fun isNotHandled(): Boolean {
        val isNeedToHandle = !hasBeenHandled
        if(!hasBeenHandled) hasBeenHandled = true
        return isNeedToHandle
    }

    fun ifNotHandledNullable(block: (t: T?) -> Unit) {
        if(isNotHandled()) {
            block.invoke(peekContent())
        }
    }

    fun ifNotHandledNotNull(block: (t: T) -> Unit) {
        if (isNotHandled()) {
            peekContent()?.let {
                block.invoke(it)
            }
        }
    }

    private fun peekContent(): T? = content
}