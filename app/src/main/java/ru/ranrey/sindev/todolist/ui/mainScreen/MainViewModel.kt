package ru.ranrey.sindev.todolist.ui.mainScreen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import ru.ranrey.sindev.todolist.domain.ToDoItemInteractor
import ru.ranrey.sindev.todolist.data.database.entities.ToDoItem

class MainViewModel(private val interactor: ToDoItemInteractor) : ViewModel() {

    var toDoItemsLiveData = MutableLiveData<List<ToDoItem>>()
    var composeDisposable = CompositeDisposable()

    init {
        refreshToDoItems()
    }

    fun refreshToDoItems() {
        composeDisposable.add(Single.fromCallable {
            interactor.getToDoItems()
        }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSuccess {
                    toDoItemsLiveData.postValue(it)
                }.subscribe())
    }

    fun editToDoItem(toDoItem: ToDoItem) {
        composeDisposable.add(Completable.fromAction {
            interactor.editToDoItem(toDoItem)
        }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnComplete {
                    refreshToDoItems()
                }.subscribe())
    }

    override fun onCleared() {
        composeDisposable.dispose()
        super.onCleared()
    }
}