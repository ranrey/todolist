package ru.ranrey.sindev.todolist.domain

import ru.ranrey.sindev.todolist.data.database.entities.ToDoItem

class ToDoItemInteractor(private val toDoItemRepository: ToDoItemRepository) {
    fun getToDoItems(): List<ToDoItem> {
        return toDoItemRepository.getToDoItems()
    }

    fun addToDoItem(toDoItem: ToDoItem) {
        toDoItemRepository.addToDoItem(toDoItem)
    }

    fun editToDoItem(toDoItem: ToDoItem) {
        toDoItemRepository.editToDoItem(toDoItem)
    }

    fun clearAll() {
        toDoItemRepository.deleteAllToDoItems()
    }
}