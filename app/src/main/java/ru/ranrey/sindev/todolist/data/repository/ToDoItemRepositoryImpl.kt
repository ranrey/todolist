package ru.ranrey.sindev.todolist.data.repository

import ru.ranrey.sindev.todolist.data.database.AppDatabase
import ru.ranrey.sindev.todolist.data.database.entities.ToDoItem
import ru.ranrey.sindev.todolist.domain.ToDoItemRepository

class ToDoItemRepositoryImpl(private val appDatabase: AppDatabase):
    ToDoItemRepository {

    override fun getToDoItems(): List<ToDoItem> {
        return appDatabase.getToDoItemDao().getToDoItems()
    }

    override fun addToDoItem(toDoItem: ToDoItem) {
        appDatabase.getToDoItemDao().insertToDoItem(toDoItem)
    }

    override fun deleteAllToDoItems() {
        appDatabase.getToDoItemDao().deleteAllToDoItems()
    }

    override fun editToDoItem(toDoItem: ToDoItem) {
        appDatabase.getToDoItemDao().updateToDoItem(toDoItem)
    }

}