package ru.ranrey.sindev.todolist.ui.util

import androidx.recyclerview.widget.DiffUtil
import ru.ranrey.sindev.todolist.data.database.entities.ToDoItem

class ToDoItemDiffUtilCallback(private val oldToDoItems: List<ToDoItem>, private val newToDoItems: List<ToDoItem>): DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldToDoItems.size
    }

    override fun getNewListSize(): Int {
        return newToDoItems.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldToDoItems[oldItemPosition].id == newToDoItems[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldToDoItems[oldItemPosition] == newToDoItems[newItemPosition]
    }
}