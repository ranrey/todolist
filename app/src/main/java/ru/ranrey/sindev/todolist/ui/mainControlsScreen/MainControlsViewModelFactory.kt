package ru.ranrey.sindev.todolist.ui.mainControlsScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.ranrey.sindev.todolist.domain.ToDoItemInteractor

class MainControlsViewModelFactory(private val interactor: ToDoItemInteractor, private val mainControlsFragmentInterface: MainControlsFragment.MainControlsFragmentInterface): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainControlsViewModel(interactor, mainControlsFragmentInterface) as T
    }
}