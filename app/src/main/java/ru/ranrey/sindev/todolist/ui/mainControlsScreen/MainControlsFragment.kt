package ru.ranrey.sindev.todolist.ui.mainControlsScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import ru.ranrey.sindev.todolist.R
import ru.ranrey.sindev.todolist.data.database.AppDatabase
import ru.ranrey.sindev.todolist.data.repository.ToDoItemRepositoryImpl
import ru.ranrey.sindev.todolist.databinding.FragmentMainControlsBinding
import ru.ranrey.sindev.todolist.domain.ToDoItemInteractor

class MainControlsFragment(private val mainControlsFragmentInterface: MainControlsFragmentInterface) :
        Fragment() {

    interface MainControlsFragmentInterface {
        fun onAddClick()
        fun onClearClick()
    }

    private val binding: FragmentMainControlsBinding by lazy {
        FragmentMainControlsBinding.inflate(layoutInflater)
    }

    private val viewModel: MainControlsViewModel by lazy {
        ViewModelProvider(
                this, MainControlsViewModelFactory(
                ToDoItemInteractor(
                        ToDoItemRepositoryImpl(
                                AppDatabase.INSTANCE!!
                        )
                ), mainControlsFragmentInterface)
        ).get(MainControlsViewModel::class.java)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        setOnClickListeners()
        initObservers()
        return binding.root
    }

    private fun setOnClickListeners() {
        binding.addButton.setOnClickListener {
            viewModel.onAddButtonClickEvent()
        }
        binding.clearButton.setOnClickListener {
            viewModel.onClearButtonClickEvent()
        }
    }

    private fun initObservers() {
        viewModel.clearButtonClickEvent.observe(viewLifecycleOwner, Observer {
            it.ifNotHandledNullable {
                viewModel.clearAll()
            }
        })
        viewModel.addButtonClickEvent.observe(viewLifecycleOwner, Observer {
            it.ifNotHandledNullable {
                mainControlsFragmentInterface.onAddClick()
            }
        })
    }
}