package ru.ranrey.sindev.todolist.data.database.dao

import androidx.room.*
import ru.ranrey.sindev.todolist.data.database.entities.ToDoItem

@Dao
interface ToDoItemDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertToDoItem(todoItem: ToDoItem)

    @Update
    fun updateToDoItem(todoItem: ToDoItem)

    @Query("SELECT * FROM ToDoItems")
    fun getToDoItems(): List<ToDoItem>

    @Query("DELETE FROM ToDoItems")
    fun deleteAllToDoItems()
}