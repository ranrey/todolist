package ru.ranrey.sindev.todolist.domain

import ru.ranrey.sindev.todolist.data.database.entities.ToDoItem

interface ToDoItemRepository {
    fun getToDoItems(): List<ToDoItem>
    fun addToDoItem(toDoItem: ToDoItem)
    fun deleteAllToDoItems()
    fun editToDoItem(toDoItem: ToDoItem)
}