package ru.ranrey.sindev.todolist.ui.addToDoScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.ranrey.sindev.todolist.domain.ToDoItemInteractor

class AddToDoViewModelFactory(private val interactor: ToDoItemInteractor, private val addToDoFragmentInterface: AddToDoFragment.AddToDoFragmentInterface): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddToDoViewModel(interactor, addToDoFragmentInterface) as T
    }

}