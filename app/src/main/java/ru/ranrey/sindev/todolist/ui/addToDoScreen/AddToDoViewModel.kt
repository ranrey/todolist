package ru.ranrey.sindev.todolist.ui.addToDoScreen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import ru.ranrey.sindev.todolist.data.database.entities.ToDoItem
import ru.ranrey.sindev.todolist.domain.ToDoItemInteractor
import ru.ranrey.sindev.todolist.ui.util.OneShootEvent

class AddToDoViewModel(private val interactor: ToDoItemInteractor, private val addToDoFragmentInterface: AddToDoFragment.AddToDoFragmentInterface) : ViewModel() {
    val acceptButtonClickEvent = MutableLiveData<OneShootEvent<Boolean>>()
    val cancelButtonClickEvent = MutableLiveData<OneShootEvent<Boolean>>()
    var compositeDisposable = CompositeDisposable()

    fun onAcceptButtonClickEvent() {
        acceptButtonClickEvent.postValue(OneShootEvent())
    }

    fun onCancelButtonClickEvent() {
        cancelButtonClickEvent.postValue(OneShootEvent())
    }

    fun addToDoItem(toDoItem: ToDoItem) {
        compositeDisposable.add(Completable.fromAction {
            interactor.addToDoItem(toDoItem)
        }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnComplete {
                    addToDoFragmentInterface.onAcceptClick()
                }.subscribe())
    }
}