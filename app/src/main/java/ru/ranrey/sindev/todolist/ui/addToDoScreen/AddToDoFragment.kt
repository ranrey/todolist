package ru.ranrey.sindev.todolist.ui.addToDoScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import ru.ranrey.sindev.todolist.data.database.AppDatabase
import ru.ranrey.sindev.todolist.data.database.entities.ToDoItem
import ru.ranrey.sindev.todolist.data.repository.ToDoItemRepositoryImpl
import ru.ranrey.sindev.todolist.databinding.FragmentAddTodoBinding
import ru.ranrey.sindev.todolist.domain.ToDoItemInteractor

class AddToDoFragment(private val addToDoFragmentInterface: AddToDoFragmentInterface) : Fragment() {

    private val binding: FragmentAddTodoBinding by lazy {
        FragmentAddTodoBinding.inflate(layoutInflater)
    }

    private val viewModel: AddToDoViewModel by lazy {
        ViewModelProviders.of(
                this, AddToDoViewModelFactory(
                ToDoItemInteractor((ToDoItemRepositoryImpl(AppDatabase.INSTANCE!!))), addToDoFragmentInterface
        )
        ).get(AddToDoViewModel::class.java)
    }

    interface AddToDoFragmentInterface {
        fun onCancelClick()
        fun onAcceptClick()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setOnClickListeners()
        initObservers()
        return binding.root
    }

    private fun setOnClickListeners() {
        binding.cancelButton.setOnClickListener {
            viewModel.onCancelButtonClickEvent()
        }
        binding.acceptButton.setOnClickListener {
            viewModel.onAcceptButtonClickEvent()
        }
        binding.editText.requestFocus()
    }

    private fun initObservers() {
        viewModel.acceptButtonClickEvent.observe(viewLifecycleOwner, {
            it.ifNotHandledNullable {
                viewModel.addToDoItem(
                    ToDoItem(
                        description = binding.editText.text.toString(),
                        status = false
                    )
                )
            }
        })
        viewModel.cancelButtonClickEvent.observe(viewLifecycleOwner, {
            it.ifNotHandledNullable {
                addToDoFragmentInterface.onCancelClick()
            }
        })
    }
}