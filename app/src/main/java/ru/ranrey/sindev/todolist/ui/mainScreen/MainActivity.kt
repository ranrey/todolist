package ru.ranrey.sindev.todolist.ui.mainScreen

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import ru.ranrey.sindev.todolist.data.database.AppDatabase
import ru.ranrey.sindev.todolist.data.repository.ToDoItemRepositoryImpl
import ru.ranrey.sindev.todolist.databinding.ActivityMainBinding
import ru.ranrey.sindev.todolist.data.database.entities.ToDoItem
import ru.ranrey.sindev.todolist.domain.ToDoItemInteractor
import ru.ranrey.sindev.todolist.ui.addToDoScreen.AddToDoFragment
import ru.ranrey.sindev.todolist.ui.mainControlsScreen.MainControlsFragment
import ru.ranrey.sindev.todolist.ui.mainControlsScreen.MainControlsFragment.MainControlsFragmentInterface
import ru.ranrey.sindev.todolist.ui.util.ToDoItemDiffUtilCallback
import ru.ranrey.sindev.todolist.ui.util.ToDoItemRecyclerAdapter

class MainActivity : AppCompatActivity() {

    private val inputMethodManager: InputMethodManager by lazy {
         applicationContext.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private lateinit var adapter: ToDoItemRecyclerAdapter

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this, MainViewModelFactory(
                ToDoItemInteractor(ToDoItemRepositoryImpl(AppDatabase.INSTANCE!!))
        )).get(MainViewModel::class.java)
    }

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private val toDoItemAdapterListener = object : ToDoItemRecyclerAdapter.OnToDoItemCheckedChangeListener {
        override fun onCheckedIsChange(isChecked: Boolean, toDoItem: ToDoItem) {
            toDoItem.status = isChecked
            viewModel.editToDoItem(toDoItem)
        }
    }

    private val mainControlsFragmentListener = object : MainControlsFragmentInterface {
        override fun onAddClick() {
            showAddToDoFragment()
        }

        override fun onClearClick() {
            viewModel.refreshToDoItems()
        }
    }

    private val addToDoFragmentListener = object : AddToDoFragment.AddToDoFragmentInterface {
        override fun onCancelClick() {
            hideAddToDoFragment()
        }

        override fun onAcceptClick() {
            hideAddToDoFragment()
            viewModel.refreshToDoItems()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showMainControlsFragment()
        setContentView(binding.root)
        setToDoItemListAdapter()
        initViewModel()
    }

    private fun setToDoItemListAdapter() {
        adapter = ToDoItemRecyclerAdapter(toDoItemAdapterListener)
        binding.listView.adapter = adapter
    }

    private fun initViewModel() {
        viewModel.toDoItemsLiveData.observe(this, {
            if (it.isEmpty()) {
                binding.emptyListText.visibility = View.VISIBLE
            } else {
                binding.emptyListText.visibility = View.GONE
            }

            val callback = ToDoItemDiffUtilCallback(
                    adapter.getData(),
                    it
            )
            val diffResult = DiffUtil.calculateDiff(callback)
            adapter.setData(it)
            diffResult.dispatchUpdatesTo(adapter)
        })
    }

    private fun showAddToDoFragment() {
        supportFragmentManager.beginTransaction().replace(binding.controls.id, AddToDoFragment(addToDoFragmentListener), null).commit()
        inputMethodManager.toggleSoftInputFromWindow(binding.root.windowToken, InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    private fun hideAddToDoFragment() {
        showMainControlsFragment()
        inputMethodManager.hideSoftInputFromWindow(binding.root.windowToken, 0)
    }

    private fun showMainControlsFragment() {
        supportFragmentManager.beginTransaction().replace(binding.controls.id, MainControlsFragment(mainControlsFragmentListener)).addToBackStack(null).commit()
    }
}