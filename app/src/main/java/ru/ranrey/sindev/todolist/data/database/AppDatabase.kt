package ru.ranrey.sindev.todolist.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ru.ranrey.sindev.todolist.data.database.dao.ToDoItemDao
import ru.ranrey.sindev.todolist.data.database.entities.ToDoItem

@Database(entities = [ToDoItem::class], version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun getToDoItemDao(): ToDoItemDao

    companion object {
        var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase? {
            if (INSTANCE == null){
                synchronized(AppDatabase::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "ToDoListDB").build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }
}