package ru.ranrey.sindev.todolist.ui.util

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.ranrey.sindev.todolist.databinding.TodoItemBinding
import ru.ranrey.sindev.todolist.data.database.entities.ToDoItem

class ToDoItemRecyclerAdapter(var onCheckedChangeListener: OnToDoItemCheckedChangeListener): RecyclerView.Adapter<ToDoItemRecyclerAdapter.ToDoItemViewHolder>() {

    interface OnToDoItemCheckedChangeListener {
        fun onCheckedIsChange(isChecked: Boolean, toDoItem: ToDoItem)
    }

    private var toDoItemList = listOf<ToDoItem>()

    fun setData(toDoItemList: List<ToDoItem>) {
        this.toDoItemList = toDoItemList
    }

    fun getData(): List<ToDoItem> {
        return toDoItemList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToDoItemViewHolder {
        return ToDoItemViewHolder(TodoItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: ToDoItemViewHolder, position: Int) {
        holder.itemViewBinding.todoDescription.text = toDoItemList[position].description
        holder.itemViewBinding.todoDescription.setOnCheckedChangeListener { compoundButton, isChecked ->
            onCheckedChangeListener.onCheckedIsChange(isChecked, toDoItemList[position])
            if(isChecked) {
                holder.itemViewBinding.todoDescription.paintFlags = holder.itemViewBinding.todoDescription.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            }
            else {
                holder.itemViewBinding.todoDescription.paintFlags = holder.itemViewBinding.todoDescription.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            }
        }
        if(toDoItemList[position].status) {
            holder.itemViewBinding.todoDescription.paintFlags = holder.itemViewBinding.todoDescription.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            holder.itemViewBinding.todoDescription.isChecked = true
        }
        else {
            holder.itemViewBinding.todoDescription.paintFlags = holder.itemViewBinding.todoDescription.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            holder.itemViewBinding.todoDescription.isChecked = false
        }
    }

    override fun getItemCount(): Int {
        return toDoItemList.size
    }

    class ToDoItemViewHolder(var itemViewBinding: TodoItemBinding): RecyclerView.ViewHolder(itemViewBinding.root) {
    }
}