package ru.ranrey.sindev.todolist.ui.mainControlsScreen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import ru.ranrey.sindev.todolist.domain.ToDoItemInteractor
import ru.ranrey.sindev.todolist.ui.util.OneShootEvent

class MainControlsViewModel(private val interactor: ToDoItemInteractor, val mainControlsFragmentInterface: MainControlsFragment.MainControlsFragmentInterface): ViewModel() {
    val addButtonClickEvent = MutableLiveData<OneShootEvent<Boolean>>()
    val clearButtonClickEvent = MutableLiveData<OneShootEvent<Boolean>>()
    var compositeDisposable = CompositeDisposable()

    fun onAddButtonClickEvent() {
        addButtonClickEvent.postValue(OneShootEvent())
    }

    fun onClearButtonClickEvent() {
        clearButtonClickEvent.postValue(OneShootEvent())
    }

    fun clearAll() {
        compositeDisposable.add(Completable.fromAction {
            interactor.clearAll()
        }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnComplete {
                    mainControlsFragmentInterface.onClearClick()
                }
                .subscribe())
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}